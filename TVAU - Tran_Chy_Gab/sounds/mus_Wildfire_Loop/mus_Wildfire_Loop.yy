{
  "compression": 1,
  "volume": 0.72,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "mus_Wildfire_Loop.wav",
  "duration": 33.8596954,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "name": "mus_Wildfire_Loop",
  "tags": [],
  "resourceType": "GMSound",
}