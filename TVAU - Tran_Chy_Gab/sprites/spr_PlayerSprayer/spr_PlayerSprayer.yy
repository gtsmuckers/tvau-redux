{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 14,
  "bbox_right": 85,
  "bbox_top": 0,
  "bbox_bottom": 144,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 100,
  "height": 144,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a9974828-de00-4165-8ddc-5fbfc72677f3","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a9974828-de00-4165-8ddc-5fbfc72677f3","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"LayerId":{"name":"55c3b001-11ef-4b17-b5ff-919d29e2bdb8","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_PlayerSprayer","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"resourceVersion":"1.0","name":"a9974828-de00-4165-8ddc-5fbfc72677f3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7c86e10a-8e5d-426c-a451-f1806a829fc3","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7c86e10a-8e5d-426c-a451-f1806a829fc3","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"LayerId":{"name":"55c3b001-11ef-4b17-b5ff-919d29e2bdb8","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_PlayerSprayer","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"resourceVersion":"1.0","name":"7c86e10a-8e5d-426c-a451-f1806a829fc3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ecbf1a05-c25e-4bb8-922e-50d87c815ffb","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ecbf1a05-c25e-4bb8-922e-50d87c815ffb","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"LayerId":{"name":"55c3b001-11ef-4b17-b5ff-919d29e2bdb8","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_PlayerSprayer","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"resourceVersion":"1.0","name":"ecbf1a05-c25e-4bb8-922e-50d87c815ffb","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_PlayerSprayer","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a530e85c-e31b-4cfc-b2a8-bbc5296d5ca4","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a9974828-de00-4165-8ddc-5fbfc72677f3","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"97b74d6d-d909-4a4d-8281-1f3d1b85895b","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7c86e10a-8e5d-426c-a451-f1806a829fc3","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9a9728b1-4518-454f-89a2-9d98ccd49090","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ecbf1a05-c25e-4bb8-922e-50d87c815ffb","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 50,
    "yorigin": 72,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_PlayerSprayer","path":"sprites/spr_PlayerSprayer/spr_PlayerSprayer.yy",},
    "resourceVersion": "1.3",
    "name": "spr_PlayerSprayer",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"55c3b001-11ef-4b17-b5ff-919d29e2bdb8","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_PlayerSprayer",
  "tags": [],
  "resourceType": "GMSprite",
}