{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 46,
  "bbox_right": 485,
  "bbox_top": 6,
  "bbox_bottom": 360,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 512,
  "height": 387,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"c6e9c3f1-8f23-4d9d-a9a0-7e75c36a32c5","path":"sprites/spr_PrepperShack/spr_PrepperShack.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c6e9c3f1-8f23-4d9d-a9a0-7e75c36a32c5","path":"sprites/spr_PrepperShack/spr_PrepperShack.yy",},"LayerId":{"name":"05ac37f3-74ae-4ca6-9255-e6a3182dc1a9","path":"sprites/spr_PrepperShack/spr_PrepperShack.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_PrepperShack","path":"sprites/spr_PrepperShack/spr_PrepperShack.yy",},"resourceVersion":"1.0","name":"c6e9c3f1-8f23-4d9d-a9a0-7e75c36a32c5","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_PrepperShack","path":"sprites/spr_PrepperShack/spr_PrepperShack.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"2b9376fb-d897-4a71-af44-27b665c2a577","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c6e9c3f1-8f23-4d9d-a9a0-7e75c36a32c5","path":"sprites/spr_PrepperShack/spr_PrepperShack.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_PrepperShack","path":"sprites/spr_PrepperShack/spr_PrepperShack.yy",},
    "resourceVersion": "1.3",
    "name": "spr_PrepperShack",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"05ac37f3-74ae-4ca6-9255-e6a3182dc1a9","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_PrepperShack",
  "tags": [],
  "resourceType": "GMSprite",
}