/// @DnDAction : YoYo Games.Common.Function
/// @DnDVersion : 1
/// @DnDHash : 3243368F
/// @DnDComment : Script assets have changed$(13_10)for v2.3.0 see $(13_10)https://help.yoyogames.com/hc/en-us/articles/360005277377 $(13_10)for more information
/// @DnDArgument : "funcName" "get_input"
function get_input() 
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7436F566
	/// @DnDParent : 3243368F
	/// @DnDArgument : "expr" "false"
	/// @DnDArgument : "var" "jump"
	jump = false;

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
	/// @DnDVersion : 1
	/// @DnDHash : 6ADB262B
	/// @DnDParent : 3243368F
	/// @DnDArgument : "key" "ord("D")"
	var l6ADB262B_0;
	l6ADB262B_0 = keyboard_check(ord("D"));
	if (l6ADB262B_0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 26E712D4
		/// @DnDParent : 6ADB262B
		/// @DnDArgument : "expr" "walk_spd"
		/// @DnDArgument : "var" "hsp"
		hsp = walk_spd;
	}

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
	/// @DnDVersion : 1
	/// @DnDHash : 5EA687BE
	/// @DnDParent : 3243368F
	/// @DnDArgument : "key" "ord("A")"
	var l5EA687BE_0;
	l5EA687BE_0 = keyboard_check(ord("A"));
	if (l5EA687BE_0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 5B5BCC02
		/// @DnDParent : 5EA687BE
		/// @DnDArgument : "expr" "-walk_spd"
		/// @DnDArgument : "var" "hsp"
		hsp = -walk_spd;
	}

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Pressed
	/// @DnDVersion : 1
	/// @DnDHash : 7A4E72DE
	/// @DnDParent : 3243368F
	var l7A4E72DE_0;
	l7A4E72DE_0 = keyboard_check_pressed(vk_space);
	if (l7A4E72DE_0)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 37FB9166
		/// @DnDParent : 7A4E72DE
		/// @DnDArgument : "expr" "true"
		/// @DnDArgument : "var" "jump"
		jump = true;
	}
}