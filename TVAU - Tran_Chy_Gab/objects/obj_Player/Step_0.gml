script_execute(get_input);

script_execute(calculate_movement);

script_execute(check_grounded);

script_execute(check_jump);

script_execute(collision);

script_execute(animation);

if (hp <= 0)
{
	global.player_lives += -1;
	room_restart();
}

if(global.player_lives <= 0)
{
	room_goto(rm_Defeat);
	global.player_lives = 3;
}