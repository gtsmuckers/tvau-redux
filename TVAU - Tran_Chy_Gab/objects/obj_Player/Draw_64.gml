// Lives HUD
_x = 5;
repeat(global.player_lives)
{
	draw_sprite_ext(
		spr_Lives,
		0,
		_x,
		7,
		0.9,
		0.9,
		0,
		c_white,
		1
		);
	_x += 30;
}

// HP bar
draw_sprite(spr_healthbar_bg, 0, 10, 36);
draw_sprite_stretched(spr_healthbar, 0, 10, 36, (hp/max_hp) * healthbar_width, healthbar_height);
draw_sprite(spr_healthbar_border, 0, 10, 36);

// Score HUD
draw_text(635, 5, "Score: " + string(global.player_score));
draw_text(700, 5, "High Score: " + string(global.high_score));