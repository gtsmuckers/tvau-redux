/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 0506D2E3
/// @DnDArgument : "var" "bbox_right"
/// @DnDArgument : "op" "3"
/// @DnDArgument : "value" "room_width"
if(bbox_right <= room_width)
{
	/// @DnDAction : YoYo Games.Movement.Set_Direction_Fixed
	/// @DnDVersion : 1.1
	/// @DnDHash : 0AD4FB03
	/// @DnDParent : 0506D2E3
	/// @DnDArgument : "direction" "0"
	direction = 0;

	/// @DnDAction : YoYo Games.Movement.Set_Speed
	/// @DnDVersion : 1
	/// @DnDHash : 7F24CEAE
	/// @DnDParent : 0506D2E3
	/// @DnDArgument : "speed" "3.3"
	speed = 3.3;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 26394D97
/// @DnDArgument : "var" "bbox_right"
/// @DnDArgument : "op" "2"
/// @DnDArgument : "value" "room_width"
if(bbox_right > room_width)
{
	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 0959F735
	/// @DnDParent : 26394D97
	instance_destroy();
}