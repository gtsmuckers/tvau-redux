//Location offsets so that it appears to be in the player's hand
x = obj_Player.x + 60
y = obj_Player.y - 10

//Sets the branch's direction to whatever the player's is
with(obj_PlayerMaskless) var l45EF20BE_0 = facing == 1;
if(l45EF20BE_0)
{
	facing = 1;
}

else

{
	facing = -1;
	//Offsets the branch to stay in the player's hand
	x = obj_Player.x - 60;
}


//sets the branch on an attack path, and plays audio
if (mouse_check_button_released(mb_left))
{
		path_start(path_BranchArc, 20, path_action_stop, false);
		
		audio_play_sound(snd_Slash, 1, false);

}
