/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 00F90EA9
instance_destroy();

/// @DnDAction : YoYo Games.Audio.Stop_Audio
/// @DnDVersion : 1
/// @DnDHash : 7276ABA3
/// @DnDArgument : "soundid" "snd_MurderHornetBuzz"
/// @DnDSaveInfo : "soundid" "snd_MurderHornetBuzz"
audio_stop_sound(snd_MurderHornetBuzz);

/// @DnDAction : YoYo Games.Audio.Play_Audio
/// @DnDVersion : 1
/// @DnDHash : 1C58C95C
/// @DnDArgument : "soundid" "snd_HornetSplat"
/// @DnDSaveInfo : "soundid" "snd_HornetSplat"
audio_play_sound(snd_HornetSplat, 0, 0);