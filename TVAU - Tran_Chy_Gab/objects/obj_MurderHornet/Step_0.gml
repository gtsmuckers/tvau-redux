/// @DnDAction : YoYo Games.Common.Execute_Script
/// @DnDVersion : 1.1
/// @DnDHash : 0942BD1D
/// @DnDArgument : "script" "animation"
/// @DnDSaveInfo : "script" "animation"
script_execute(animation);

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 5D172F5F
/// @DnDArgument : "var" "distance_to_object(obj_PlayerMaskless)"
/// @DnDArgument : "op" "3"
/// @DnDArgument : "value" "150"
if(distance_to_object(obj_PlayerMaskless) <= 150)
{
	/// @DnDAction : YoYo Games.Movement.Set_Direction_Point
	/// @DnDVersion : 1
	/// @DnDHash : 52CA73DC
	/// @DnDParent : 5D172F5F
	/// @DnDArgument : "x" "obj_PlayerMaskless.x"
	/// @DnDArgument : "y" "obj_PlayerMaskless.y"
	direction = point_direction(x, y, obj_PlayerMaskless.x, obj_PlayerMaskless.y);

	/// @DnDAction : YoYo Games.Movement.Set_Speed
	/// @DnDVersion : 1
	/// @DnDHash : 0D208168
	/// @DnDParent : 5D172F5F
	/// @DnDArgument : "speed" "3"
	speed = 3;

	/// @DnDAction : YoYo Games.Audio.Play_Audio
	/// @DnDVersion : 1
	/// @DnDHash : 6E9400DC
	/// @DnDParent : 5D172F5F
	/// @DnDArgument : "soundid" "snd_MurderHornetBuzz"
	/// @DnDArgument : "loop" "1"
	/// @DnDSaveInfo : "soundid" "snd_MurderHornetBuzz"
	audio_play_sound(snd_MurderHornetBuzz, 0, 1);
}