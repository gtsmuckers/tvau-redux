event_inherited(); //so it will inherit from par_speaker

myPortrait			= spr_PortraitPrepper;
myVoice				= snd_SpeechMale;
myFont				= fnt_dialogue;
myName				= "Prepper";

choice_variable		= -1;	//the variable we change depending on the player's choice in dialogue