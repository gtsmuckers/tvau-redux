var l66915CB9_0 = room;
switch(l66915CB9_0)
{
	case rm_Game:
		var l203A8CF6_0 = sprite_get_width(spr_Lives);
		var l203A8CF6_1 = 0;
		if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
		for(var l203A8CF6_2 = __dnd_lives; l203A8CF6_2 > 0; --l203A8CF6_2) {
			draw_sprite(spr_Lives, 0, x + x + l203A8CF6_1, y + y);
			l203A8CF6_1 += l203A8CF6_0;
		}
		break;

	case rm_Menu:
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		draw_set_colour($FF0F0FFF & $ffffff);
	
		draw_text_transformed(400, 150, string("THE VIRUS AMONGST US") + "", 3, 3, 0);
	
		draw_set_colour($FFFFFFFF & $ffffff);
	
		draw_text(400, 230, string("Make it to the other side... alive!") + "");
	
		draw_text(400, 280, string("Use A and D to move") + "");
	
		draw_text(400, 330, string("Press Space to jump") + "");
	
		draw_text(400, 380, string("Press Left-Click to attack") + "");
		
		draw_text(400, 430, string("Press Escape to pause") + "");
	
		draw_set_colour($FF0F0FFF & $ffffff);
	
		draw_text(400, 480, string("PRESS ENTER TO START...") + "");
	
		draw_set_colour($FFFFFFFF & $ffffff);
	
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		break;

	case rm_Defeat:
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		draw_set_colour($FF282DFF & $ffffff);
	
		draw_text_transformed(400, 200, string("Little did you know this day would be your last...") + "", 2, 2, 0);
	
		draw_set_colour($FFFFFFFF & $ffffff);
	
		draw_text(400, 300, string("PRESS ENTER TO TRY AGAIN... ") + "");
	
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		break;

	case rm_Victory:
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		draw_set_colour($FF21FF21 & $ffffff);
	
		draw_text_transformed(400, 200, string("You made it safely through another day!") + "", 2, 2, 0);
	
		draw_set_colour(c_orange);
		draw_text_transformed(315, 250, string("Score: ") + string(global.player_score), 2, 2, 0);
		
		draw_set_colour(c_red);
		draw_text_transformed(485, 250, string("High Score: ") + string(global.high_score), 2, 2, 0);
	
		draw_set_colour($FFFFFFFF & $ffffff);
	
		draw_text(400, 300, string("PRESS ENTER TO PLAY AGAIN") + "");
	
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		break;
}