if(room == rm_Menu)
{
	audio_stop_all();
	
	audio_play_sound(mus_Idle_Song, 0, 1);
}

if(room == rm_LevelOneWildfire)
{
	audio_stop_all();

	audio_play_sound(mus_Wildfire_Loop, 1, 1);
}

if(room == rm_LevelTwoCountry)
{
	audio_stop_all();

	audio_play_sound(mus_Countryside, 1, 1);
}

if(room == rm_Victory)
{
	
	audio_stop_all();
	
	audio_play_sound(snd_Victory, 0, 0);
}

if(room == rm_Defeat)
{
	
	audio_stop_all();
	
	audio_play_sound(snd_Defeat, 0, 0);
}
