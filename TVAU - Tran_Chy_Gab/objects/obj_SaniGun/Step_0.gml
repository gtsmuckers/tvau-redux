image_angle = point_direction(obj_Player.x, obj_Player.y, mouse_x, mouse_y);



var pushX = lengthdir_x(push,image_angle -180) ;
var pushY = lengthdir_y(push,image_angle -180) ;


x = lerp(obj_Player.x, obj_Player.x + pushX, 0.6);
y = lerp(obj_Player.y, obj_Player.y + pushY, 0.6);

push -=1;
push = clamp(push, 0, 10);


if (mouse_check_button_released(mb_left))
{
	push = 10;
	audio_play_sound(snd_GunSquirt, 0, false);
	
	var bullet = instance_create_layer(x,y, "Instances", obj_SaniBullet );	
	bullet.image_angle = image_angle;
	bullet.direction = image_angle;
	bullet.speed = 6;
}