//move back and forth
if place_meeting(x+(xspd*8), y, obj_InvisibleWall){
	while !place_meeting(x+sign(xspd), y, obj_InvisibleWall){
		x += sign(xspd);
	}
	
	if (canTurn){
		canTurn = false;
		alarm[0] = room_speed*1;
		xspd *= -1;
	}
}

if !place_meeting(x+(xspd*10),y+5,obj_InvisibleWall){
	xspd *= -1;
}

//attack player
if place_meeting(x+(xspd*50), y, obj_Player){
	if (canAttack){
		canAttack = false;
		audio_play_sound(snd_DoorSlam, 1, false);
		xspd = 0;
		alarm[1] = room_speed*0.45;
	}
}

//move
x += xspd;

//face direction of movement
if !xspd = 0{
	image_xscale = sign(xspd) * -1;
}

if hp <= 0{
	global.player_score += 5;
	instance_destroy();
}