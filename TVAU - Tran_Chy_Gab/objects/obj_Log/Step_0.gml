/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 3D950BC6
/// @DnDArgument : "var" "x"
/// @DnDArgument : "op" "3"
/// @DnDArgument : "value" "obj_PlayerMaskless.x"
if(x <= obj_PlayerMaskless.x)
{
	/// @DnDAction : YoYo Games.Movement.Set_Direction_Fixed
	/// @DnDVersion : 1.1
	/// @DnDHash : 7B7E8D3A
	/// @DnDParent : 3D950BC6
	/// @DnDArgument : "direction" "270"
	direction = 270;

	/// @DnDAction : YoYo Games.Movement.Set_Speed
	/// @DnDVersion : 1
	/// @DnDHash : 61790158
	/// @DnDParent : 3D950BC6
	/// @DnDArgument : "speed" "12"
	speed = 12;
}