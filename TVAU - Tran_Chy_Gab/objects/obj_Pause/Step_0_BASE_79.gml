//return to game

if keyboard_check_pressed(vk_escape){
	room_goto(global.roomfrom);
}

//also return to game, but completely
if keyboard_check_pressed(ord("R")){
	instance_create_layer(x,y,"instances",obj_Restart);
	room_goto(global.roomfrom);
}