draw_set_halign(fa_center);
draw_set_font(fnt_Text);

//line spacing
var _gap = 40;

//draw items
for (var i = 0; i < array_length(menu); ++i) {
	draw_set_color(c_white);
	if i == index draw_set_color($FF282DFF);
	
	draw_text(room_width/2, room_height + .4 + _gap * i, menu[i]);
}

